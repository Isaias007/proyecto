<?php

require_once "core/App.php";

class Conection
{


    public static function make()
    {
        try {

            $config = App::get("config")["database"];

            $connection = new PDO(

                $config["connection"] . ";dbname=" . $config["name"],

                $config["username"],

                $config["password"],

                $config["options"]
            );


        /*
        
        ¿Cuál es la ventaja de tener un archivo config.php aparte?

        La ventaja de tener el config aparte esq la organisacion y la rapidez para encontrar las configuraciones cambia y gracias a ello podemos no instaciar tantas veces lo mismo
        que consume memoria y asi bajar el consumo de recursos.

        Basicamente centralizamos las conexion y la peticion de datos co respecto a la configuracion
        
        
        
        */
        } catch (PDOException $PDOException) {

            throw new AppException("No se ha podido conectar con la BBDD");
        }

        return $connection;
    }
}

?>