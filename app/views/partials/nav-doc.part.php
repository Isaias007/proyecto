<!-- Navigation Bar -->
<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class="<?php if (utilidad("index")){echo "active";} ?> lien"><a href=<?php if (utilidad("index")){echo "#";}else{echo "index";} ?>> <i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class="<?php if (utilidad("about")){echo "active";} ?> lien"><a href=<?php if (utilidad("about")){echo "#";}else{echo "about";} ?>><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class="<?php if (utilidad("blog")){echo "active";} ?> lien"><a href=<?php if (utilidad("blog")){echo "#";}else{echo "blog";} ?>><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class="<?php if (utilidad("contact")){echo "active";} ?> lien"><a href=<?php if (utilidad("contact")){echo "#";}else{echo "contact";} ?>><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class="<?php if (utilidad("galeria")){echo "active";} ?> lien"><a href=<?php if (utilidad("galeria")){echo "#";}else{echo "galeria";} ?>><i class="fa fa-image"></i> Galeria</a></li>
              <li class="<?php if (utilidad("Asociados")){echo "active";} ?> lien"><a href=<?php if (utilidad("Asociados")){echo "#";}else{echo "asociados";} ?>><i class="fa fa-hand-o-right"></i> Asociados</a></li>
            </ul>
         </div>
     </div>
   </nav>
<!-- End of Navigation Bar -->