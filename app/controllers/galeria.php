<?php


try {

    // Conseguimos del contenedor la conexion con un getter
    $imagenGaleriaRepository = new ImagenGaleriaRepository();

    $categoriaRepository = new CategoriaRepository();

    $categorias = $categoriaRepository->findAll();
    $imagenes = $imagenGaleriaRepository->findAll();

    $categoria = "";

    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);

        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getName(), $categoria, $descripcion);

        $imagenGaleriaRepository->save($imagenGaleria);

        $mensaje = "Se ha guardado la imagen en la BBDD.";

        App::get("logger")->add($mensaje);
    }

    FlashMessage::unset("descripcion");

    FlashMessage::unset("categoria");

    $descripcion = "";

    $categoria = "";



    //Capturamos los errores 
} catch (FileException $fileException) {

    FlashMessage::set("errores", [$fileException->getMessage()]);
} catch (AppException $appException) {

    FlashMessage::set("errores", [$appException->getMessage()]);
} catch (QueryException $queryException) {

    FlashMessage::set("errores", [$queryException->getMessage()]);
} catch (NotFoundException $notFound) {

    FlashMessage::set("errores", [$notFound->getMessage()]);
}

$errores = FlashMessage::get("errores");








require __DIR__ . "/../views/gallery.view.php";
