<?php
$errores = array();



if (isset($_POST["nombre"]) && isset($_POST["correo"]) && isset($_POST["titulo"])) {

    if (strlen($_POST["nombre"]) == 0 && strlen($_POST["correo"]) == 0 && strlen($_POST["titulo"]) == 0) {
        array_push($errores, "Faltan todos los parametros");
    } else if (strlen($_POST["nombre"]) == 0) {
        array_push($errores, "Falta escribir el nombre");
    } else if (strlen($_POST["correo"]) == 0) {
        array_push($errores, "Falta escribir el correo");
    } else if (strlen($_POST["titulo"]) == 0) {
        array_push($errores, "Falta escribir un titulo");
    } else if (strpos($_POST["correo"], "@") == false) {
        array_push($errores, "El correo esta mal escrito");
    }


    require __DIR__ . "/../views/contact.view.php";
} else {
    $_POST["nombre"] = '  ';
    $_POST["apellidos"] = '  ';
    $_POST["correo"] = '  ';
    $_POST["titulo"] = '  ';
    $_POST["mensaje"] = '  ';

    require __DIR__ . "/../views/contact.view.php";
}
