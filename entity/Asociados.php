<?php
require_once "database/IEntity.php";


class Asociados implements IEntity{

    private $nombre;

    private $logo;

    private $descripcion;

    const RUTA_IMAGENES_LOGO = "images/index/";

    public function __construct($nombre, $logo, $descripcion)
    {
        
        $this->nombre = $nombre;

        $this->logo = $logo;

        $this->descripcion = $descripcion;




    }



    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of logo
     */ 
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getURLlogo() : string{

        return self::RUTA_IMAGENES_LOGO;

    }



    public function toArray(): array
    {
        return[
            "nombre"=>$this->getNombre(),

            "logo"=>$this->getLogo(),

            "descripcion"=>$this->getDescripcion()
        ];
        
    }


}
