<?php

class Categoria implements IEntity
{

    private $id;

    private $nombre;

    private $numImagenes;

    public function __construct($id = 0, $nombre = "", $numImagenes = 0)
    {

        $this->id = $id;

        $this->nombre = $nombre;

        $this->numImagenes = $numImagenes;
    }






    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of numImagenes
     */
    public function getNumImagenes()
    {
        return $this->numImagenes;
    }

    public function toArray(): array
    {
        return [
            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "numImagenes" => $this->getNumImagenes()
        ];
    }
}
