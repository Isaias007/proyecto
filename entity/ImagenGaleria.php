<?php

require_once "database/IEntity.php";

class ImagenGaleria implements IEntity{

    const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALERIA = "images/index/gallery/";

    private $id;

    private $nombre;

    private $categoria;
    
    private $descripcion;

    private $numVisualizaciones;

    private $numLikes;

    private $numDownloads;



    public function __construct($nombre="", $categoria=0, $descripcion="", $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0)
    
    {
        $this->id = null;

        $this->nombre = $nombre;

        $this->categoria = $categoria;

        $this->descripcion = $descripcion;

        $this->numVisualizaciones = $numVisualizaciones;

        $this->numLikes = $numLikes;
        
        $this->numDownloads = $numDownloads;

    }




     /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    
    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    
    /**
     * Get the value of numVisualizaciones
     */ 
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }


    /**
     * Get the value of numLikes
     */ 
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Get the value of numDownloads
     */ 
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }
    

    public function __toString(){

        return  $this->descripcion; 

    }
    
   
   
    
    
    public function getURLPortfolio() : string{

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

    }


    public function getURLGallery() : string{

        return self::RUTA_IMAGENES_GALERIA . $this->getNombre();

    }
    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }


    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }


    public function toArray(): array
    {
        return [
            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),
            
            "categoria"=>$this->getCategoria(),

            "descripcion"=>$this->getDescripcion(),

            "numVisualizaciones"=>$this->getNumVisualizaciones(),
           
            "numLikes"=>$this->getNumLikes(),
           
            "numDownloads"=>$this->getNumDownloads()
        ];
    }



    
}
